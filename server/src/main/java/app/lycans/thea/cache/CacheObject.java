package app.lycans.thea.cache;

public abstract class CacheObject {
    public boolean isCached = false;
    public final void setIsCached(boolean isCached){
        this.isCached = isCached;
    }
}
