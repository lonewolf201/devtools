package app.lycans.thea.cache;

import java.io.Serializable;
import java.util.HashMap;

public class CacheMap extends CacheObject implements Serializable{
    private HashMap<String,Object> cacheMap = new HashMap<>();

    public void put(String key,Object value){
        if(Serializable.class.isAssignableFrom(value.getClass())){
            cacheMap.put(key,value);
        }
    }

    public Object get(String key){
        return cacheMap.get(key);
    }
}
