package app.lycans.thea.cache;

import redis.clients.jedis.BinaryJedis;
import redis.clients.jedis.HostAndPort;

import java.io.*;


public class ObjectRedisCache {
    private volatile static ObjectRedisCache objectRedisCache;
    private BinaryJedis binaryJedis;
    private ObjectRedisCache(String url, int port){
        binaryJedis = new BinaryJedis(new HostAndPort(url,port));
    }

    public static ObjectRedisCache getInstance(){
        if(objectRedisCache==null){
            objectRedisCache = new ObjectRedisCache("localhost",6379);
        }
        return objectRedisCache;
    }

    public void setCacheObject(String key,CacheObject cacheObject){
        byte[] serializedObj = serializeToByteArray(cacheObject);
        binaryJedis.set(key.getBytes(),serializedObj);
    }

    public byte[] serializeToByteArray(CacheObject cacheObject){
        cacheObject.setIsCached(true);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try{
            out = new ObjectOutputStream(bos);
            out.writeObject(cacheObject);
        }catch (IOException io){
            cacheObject.setIsCached(false);
            io.printStackTrace();
        }
        return bos.toByteArray();
    }

    public CacheObject getCacheObject(String key){
        byte[] byteArray = binaryJedis.get(key.getBytes());
        return deserializeToCacheObject(byteArray);
    }

    public CacheObject deserializeToCacheObject(byte[] byteArray){
        ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
        ObjectInput in = null;
        CacheObject cacheObject=null;
        try{
            in = new ObjectInputStream(bis);
            cacheObject=(CacheObject) in.readObject();
        }catch (IOException | ClassNotFoundException io){
             io.printStackTrace();
        }
        return cacheObject;
    }

}
