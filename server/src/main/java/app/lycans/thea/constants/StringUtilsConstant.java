package app.lycans.thea.constants;

public class StringUtilsConstant {
    public static final String SPLIT_STRING = "splitString";

    //REQUEST PARAMS

    public static final String PARAM_SOURCE_STRING = "srcString";
    public static final String PARAM_REGEX = "regex";
    public static final String PARAM_STRING_LIST = "stringList";
    public static final String PARAM_IS_ASC = "isAsc";


    //OPERATIONS

    public static final String OPERATION_SPLIT_STRING = "SPLIT_STRING";
    public static final String OPERATION_SORT_STRING = "SORT_STRING";



    public static final String RESPONSE_SORTED_LIST = "SORTED_LIST";
}
