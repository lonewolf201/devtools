package app.lycans.thea.constants;

public class ConfigConstants {
    public static final String NAV_BAR_PROPERTIES="nav-bar-properties.xml";
    public static final String FILTER_CONFIG="filter-config.xml";
    public static final String TOOL_CONFIG="tools.xml";
}
