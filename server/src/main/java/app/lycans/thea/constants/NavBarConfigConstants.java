package app.lycans.thea.constants;

public class NavBarConfigConstants {
    public static final String DISPLAY_ORDER="DISPLAY_ORDER";
    public static final String DISPLAY_NAME="DISPLAY_NAME";
    public static final String ID="ID";
    public static final String LINK_TO="LINK_TO";
    public static final String ICON="icon";
}
