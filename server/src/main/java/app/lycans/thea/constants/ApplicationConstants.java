package app.lycans.thea.constants;

public class ApplicationConstants {
    public static final String CONFIGS = "/configs";
    public static final String IS_APP_SERVED = "isAppServed";

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_LENGTH = "Content-Length";


    private ApplicationConstants() {
    }
}
