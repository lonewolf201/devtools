package app.lycans.thea.filter;

import app.lycans.thea.constants.ApplicationConstants;

import app.lycans.thea.util.ConfigUtil;
import org.json.JSONObject;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.regex.Pattern;

@Component
@Order(1)

public class ApplicationEntryFilter implements Filter {
    Pattern excludePattern;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        JSONObject params = ConfigUtil.getFilterConfigs().getConfigForFilterName(this.getClass().getName()).getJSONObject("params");
        this.excludePattern = Pattern.compile(params.getString("exclude-pattern"));
    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = ((HttpServletRequest)servletRequest);
        String url = req.getRequestURI();
        boolean isAppServed = req.getParameterMap().containsKey(ApplicationConstants.IS_APP_SERVED);
        if( isAppServed || excludePattern.matcher(url).matches()){
            filterChain.doFilter(servletRequest,servletResponse);
        }else{
            req.getServletContext().getRequestDispatcher("/").forward(servletRequest,servletResponse);
        }
    }
}
