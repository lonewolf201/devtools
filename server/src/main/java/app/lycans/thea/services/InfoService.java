package app.lycans.thea.services;


import app.lycans.thea.configs.NavBarConfigs;
import app.lycans.thea.constants.ConfigConstants;
import app.lycans.thea.constants.IndexConstants;
import app.lycans.thea.util.ConfigUtil;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class InfoService {


    public HashMap<String, Object> getIndexContent(){
        HashMap<String,Object> indexContent = new HashMap<>();
        HashMap<String,HashMap<String,Object>> navBarContent = getNavBarContent();
        indexContent.put(IndexConstants.NAV_BAR_CONTENT,navBarContent);
        return indexContent;
    }

    public HashMap<String,HashMap<String, Object>> getNavBarContent(){
        NavBarConfigs navBarConfigs = ConfigUtil.getConfig(NavBarConfigs.class, ConfigConstants.NAV_BAR_PROPERTIES);
        return navBarConfigs.getConfigs();
    }
}
