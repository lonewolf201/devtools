package app.lycans.thea.services;

import app.lycans.thea.constants.StringUtilsConstant;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StringUtilsService {

    public Map<String,Object> splitString(String sourceString,String regex){
        Map<String,Object> result = new HashMap<>();
        String[] resultantString = sourceString.split(regex);
        result.put(StringUtilsConstant.SPLIT_STRING,resultantString);
        return result;
    }

    public ArrayList<String> sortList(String stringList,boolean isAsc){
        ArrayList<String> unsortedStrings = (ArrayList<String>) getArrayListFromString(stringList);
        Comparator<String> comp = isAsc?null:Collections.reverseOrder();
        unsortedStrings.sort(comp);
        return unsortedStrings;
    }

    private ArrayList<?> getArrayListFromString(String stringList){
        JSONArray stringArray = new JSONArray(stringList);
        return (ArrayList<?>) stringArray.toList();
    }
}
