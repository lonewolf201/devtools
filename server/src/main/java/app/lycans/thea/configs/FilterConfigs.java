package app.lycans.thea.configs;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class FilterConfigs {

    private JSONObject filterConfig;

    public FilterConfigs(Document document){
        this.filterConfig = new JSONObject();
        NodeList filters = document.getElementsByTagName("filter");
        for(int i=0;i<filters.getLength();i++){
            Element element = (Element) filters.item(i);
            String filterClass = element.getAttribute("className");
            JSONObject filterSpecs = getFilterSpecs(element);
            filterConfig.put(filterClass,filterSpecs);
        }
    }

    public JSONObject getFilterSpecs(Element element){
        JSONObject filterSpecs = new JSONObject();
        filterSpecs.put("params",getFilterParams(element));
        return filterSpecs;
    }

    public JSONObject getFilterParams(Element element){
        JSONObject paramsObject = new JSONObject();
        NodeList paramsParent = element.getElementsByTagName("params");
        if(paramsParent!=null){
            Element params = (Element) paramsParent.item(0);
            NodeList paramList = params.getElementsByTagName("param");
            for(int j=0;j<paramList.getLength();j++){
                Element paramElement = (Element) paramList.item(j);
                String paramName = paramElement.getAttribute("name");
                String paramValue = paramElement.getTextContent();
                paramsObject.put(paramName,paramValue);
            }
        }
        return paramsObject;
    }

    public JSONObject getConfigForFilterName(String className){
        return filterConfig.getJSONObject(className);
    }
}
