import Route from '@ember/routing/route';
import Ajax from '../utils/ajax-util';
export default Ember.Route.extend({
  router: Ember.inject.service('-routing'),
  universalService: Ember.inject.service('universal-service'),
  setActiveRoute(controllerData) {
    let path = window.location.pathname;
    let routeMap = controllerData.routeMap;
    let route = routeMap[path.replace('/', '')];
    controllerData.activeTab = route ? route.ID : 'TAB01';
  },
  model(controller) {
    let self = this;
    let url = '/info';
    let controllerData = {};
    Ajax.sendRequest(url, 'GET', this, null)
      .then(function (json) {
        let navBarContent = json.navBarContent;
        self.get('universalService').setRoutes(navBarContent);
        controllerData.routes = Object.values(navBarContent);
        controllerData.routes.sort(function (routeA, routeB) {
          let displayOrderA = routeA.DISPLAY_ORDER;
          let displayOrderB = routeB.DISPLAY_ORDER;
          return displayOrderB > displayOrderA ? -1 : 1;
        });
        controllerData.routeMap = navBarContent;
        self.setActiveRoute(controllerData);
        self.controller.set('controllerData', controllerData);
        self.controller.set('isLoading', false);
      })
      .catch(function (xhr) {
        console.log(xhr);
      });
  },
});
