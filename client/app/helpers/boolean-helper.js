export default Ember.Helper.helper(function (params) {
  var operation = params[0];
  if (operation === '==') {
    return params[1] == params[2];
  } else if (operation === '||') {
    return params[1] || params[2];
  } else if (operation === '!'){
    return !params[1];
  }
});
