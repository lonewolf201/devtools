import Controller from '@ember/controller';

export default Ember.Controller.extend({
  actions:{
    encodeUri:function(){
      let decode = $("#decodedUri").val();
      let decoded = encodeURIComponent(decode);
      $("#encodedUri").val(decoded);
    },
    decodeUri:function(){
      let encode = $("#encodedUri").val();
      let encoded = decodeURIComponent(encode);
      $("#decodedUri").val(encoded);
    },
    clear:function(){
      $("#decodedUri").val("");
      $("#encodedUri").val("");
    }
  }
});
