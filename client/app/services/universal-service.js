import Service from '@ember/service';

export default Ember.Service.extend({
  routes: {},
  setRoutes: function (routes) {
    this.set('routes', routes);
  },
  getRoutes: function () {
    return this.routes;
  },
});
