import Controller from '@ember/controller';
import prettyPrintJson from'pretty-print-json';


export default Ember.Controller.extend({
  result: '',
  json: '',
  actions: {
    submitForFormat: function () {
      let jsonInput = $('#inputJson').val();
      try{
        let json = JSON.parse(jsonInput);
        $("#outputJson").html(prettyPrintJson.prettyPrintJson.toHtml(json));
      }catch(err){
        alert("Your input json has a syntax error");
      }
    },
    clear: function () {
      $('#inputJson').val('');
      $("#outputJson").html(null);
      //        let options = this.options;
      //        Object.keys(options).forEach(function (pos) {
      //          options[pos] = false;
      //        });
      //        $('.pretty.p-switch input').each(function () {
      //          $(this).prop('checked', false);
      //        });
      //        this.set('options', options);
    },
  },
});
